import App from "./"
import {createApp} from 'vue'

const app = createApp(App)
app.mount('#app')