import { createApp } from 'vue'
import App from './App.vue'

const app=createApp(App)
import {Button} from 'v3-componernts-wsj'
app.use(Button)

import MyCard from './packages/card/card'
app.component(MyCard.name,MyCard)
app.mount('#app')
// import store from './store'

// createApp(App).use(store).mount('#app')
